dnsdist (1.8.2-1+wmf12u2) bookworm-wikimedia; urgency=medium

  * Add patch to disable topQueries() during build
  * Typo in last changelog: backport is to Debian bookworm, not bullseye

 -- Sukhbir Singh <ssingh@wikimedia.org>  Wed, 01 Nov 2023 12:11:16 -0400

dnsdist (1.8.2-1+wmf12u1) bookworm-wikimedia; urgency=medium

  * Backport of upstream version 1.8.2 to debian bullseye.

 -- Sukhbir Singh <ssingh@wikimedia.org>  Tue, 31 Oct 2023 12:41:22 -0400

dnsdist (1.8.0-1+wmf12u1) bookworm-wikimedia; urgency=medium

  * Bump version for bookworm release.

 -- Sukhbir Singh <ssingh@wikimedia.org>  Wed, 26 Jul 2023 12:41:43 -0400

dnsdist (1.8.0-1+wmf11u1) bullseye-wikimedia; urgency=medium

  * Backport of upstream version 1.8.0 to debian bullseye.

 -- Sukhbir Singh <ssingh@wikimedia.org>  Thu, 29 Jun 2023 11:19:44 -0400

dnsdist (1.8.0-1) unstable; urgency=medium

  * New upstream version 1.8.0 (Closes: #1037620)

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 15 Jun 2023 17:04:13 +0200

dnsdist (1.7.3-2) unstable; urgency=medium

  [ Manuel A. Fernandez Montecelo ]
  * Add support for riscv64 arch (Closes: #1025081)

  [ Chris Hofstaedtler ]
  * Build-Depend: architecture-is-64-bit instead of hardcoded arch list

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 20 Dec 2022 07:49:05 +0000

dnsdist (1.7.3-1) unstable; urgency=medium

  * New upstream version 1.7.3

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 01 Nov 2022 22:35:46 +0000

dnsdist (1.7.2-2) unstable; urgency=medium

  * Set explicit architecture list.
    A better solution than an explicit arch list or letting builds fail on
    each upload would be very welcome.
  * Use luajit on arm64

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 01 Nov 2022 16:34:49 +0000

dnsdist (1.7.2-1) unstable; urgency=medium

  * New upstream version 1.7.2

 -- Chris Hofstaedtler <zeha@debian.org>  Wed, 15 Jun 2022 14:04:47 +0000

dnsdist (1.7.1-1) unstable; urgency=medium

  * New upstream version 1.7.1

 -- Chris Hofstaedtler <zeha@debian.org>  Wed, 27 Apr 2022 07:17:37 +0000

dnsdist (1.7.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Remove unused license definitions for Unlicense.

  [ Chris Hofstaedtler ]
  * New upstream version 1.7.0
  * Build with nghttp2

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 23 Jan 2022 15:15:34 +0000

dnsdist (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1
  * Update debian/copyright, other metadata

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 19 Sep 2021 14:56:40 +0000

dnsdist (1.6.0-2) unstable; urgency=medium

  * Upload to unstable.
  * Disable tests, as they require a working IPv4 network stack
  * Note: upstream dropped support for archs with 32bit time_t values
    in 1.6.0.

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 20 Aug 2021 16:38:14 +0000

dnsdist (1.6.0-1) experimental; urgency=medium

  * New upstream version 1.6.0
    * Remove upstream applied patches
  * configure: explicitly pass --with-lmdb

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 13 May 2021 09:59:55 +0000

dnsdist (1.5.1-3) unstable; urgency=medium

  * Disable GnuTLS, one crypto lib is enough

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 18 Dec 2020 14:12:25 +0000

dnsdist (1.5.1-2) unstable; urgency=medium

  * Simplify build rules
  * Bump Standards-Version to 4.5.1
  * Build with luajit on amd64, and Lua 5.1 on other archs.
    Once #908137 is fixed, we should enable luajit on arm64 too.
    Unfortunately we were building with Lua 5.3 earlier, so this
    is a feature "regression" of sorts - but it can be much faster.
  * Add upstream patch fixing an eBPF long qname issue

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 17 Dec 2020 22:20:04 +0000

dnsdist (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 01 Oct 2020 13:06:31 +0000

dnsdist (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0
  * Update ownership rules for /etc/dnsdist/dnsdist.conf
  * Update Standards-Version to 4.5.0, dh compat to 13

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 30 Jul 2020 22:49:16 +0000

dnsdist (1.4.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.

  [ Chris Hofstaedtler ]
  * Add upstream patch to fix FTBFS with gcc 10 (Closes: #957145)
  * d/copyright: rename GPL-3-or-Autoconf license paragraph

 -- Chris Hofstaedtler <zeha@debian.org>  Wed, 22 Jul 2020 21:50:19 +0000

dnsdist (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0

 -- Chris Hofstaedtler <zeha@debian.org>  Wed, 20 Nov 2019 17:47:14 +0000

dnsdist (1.4.0~rc5-1) unstable; urgency=medium

  * New upstream version 1.4.0~rc5
  * Bump Standards-Version to 4.4.1

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 05 Nov 2019 19:43:53 +0000

dnsdist (1.4.0~rc3-1) unstable; urgency=medium

  * New upstream version 1.4.0~rc3

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 01 Oct 2019 12:02:39 +0000

dnsdist (1.4.0~rc2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures.
  * Set fields Upstream-Contact in debian/copyright.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

  [ Chris Hofstaedtler ]
  * New upstream version 1.4.0~rc2
    Build with CDB and LMDB for new features.

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 02 Sep 2019 19:39:24 +0000

dnsdist (1.4.0~rc1-1) unstable; urgency=medium

  * New upstream version 1.4.0~rc1

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 30 Aug 2019 11:11:52 +0000

dnsdist (1.4.0~beta1-3) unstable; urgency=medium

  * d/rules: remove harmless extra argument to dh_auto_configure
  * Update init script.
    Thanks to Jens Meißner <heptalium@gmx.de> (Closes: #934162)

 -- Chris Hofstaedtler <zeha@debian.org>  Wed, 07 Aug 2019 22:38:35 +0000

dnsdist (1.4.0~beta1-2) unstable; urgency=medium

  * Build with libcap
  * d/control, d/rules: clean up and sync with pdns(-recursor)
  * Enable DNS-over-HTTPS support
  * Reenable RE2
  * Filter net-snmp-config flags, see #870734
  * Ship sysvinit script (Closes: #832136)

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 21 Jul 2019 20:04:10 +0000

dnsdist (1.4.0~beta1-1) unstable; urgency=medium

  * New upstream version 1.4.0~beta1
  * Build-Depend on systemd for proper version detection
  * Bump Standards-Version to 4.4.0
  * Use debhelper-compat v12
  * Ship DEP12 UpstreamMetadata per user request
  * d/copyright: Update
  * d/control: Set Rules-Requires-Root: no
  * d/rules: use variables from pkg-info.mk
  * d/rules: stop removing RestrictAddressFamilies from systemd service file
  * d/rules: fix trailing whitespace
  * Install SNMP MIB

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 21 Jul 2019 15:18:15 +0000

dnsdist (1.3.3-3) unstable; urgency=medium

  * Enable SNMP support

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 12 Feb 2019 08:54:00 +0000

dnsdist (1.3.3-2) unstable; urgency=medium

  * Set secpoll suffix and add default config file to turn it off

 -- Chris Hofstaedtler <zeha@debian.org>  Sat, 10 Nov 2018 14:49:21 +0000

dnsdist (1.3.3-1) unstable; urgency=medium

  * New upstream version 1.3.3, including fix for CVE-2018-14663
    (Closes: #913231).

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 09 Nov 2018 19:34:32 +0000

dnsdist (1.3.2-1) unstable; urgency=medium

  * New upstream version 1.3.2
  * Bump Standards-Version to 4.1.5
  * Drop upstream applied patches

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 23 Jul 2018 07:16:30 +0000

dnsdist (1.3.0-4) unstable; urgency=medium

  * Update test-dnsdistrings patch to avoid further s390x failure

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 03 Apr 2018 11:40:14 +0000

dnsdist (1.3.0-3) unstable; urgency=medium

  * Avoid hard test failure for a soft condition

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 03 Apr 2018 07:57:07 +0000

dnsdist (1.3.0-2) unstable; urgency=medium

  * Add patch from upstream to fix test/build failure

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 30 Mar 2018 11:49:47 +0000

dnsdist (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0
  * Enable DNS-over-TLS and libfstrm.

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 30 Mar 2018 08:45:33 +0000

dnsdist (1.2.1-1) unstable; urgency=medium

  * New upstream version 1.2.1
  * Avoid debian-rules-is-dh_make-template lintian error

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 16 Feb 2018 10:41:23 +0000

dnsdist (1.2.0-3) unstable; urgency=medium

  * Enable tests at build time

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 19 Jan 2018 21:58:27 +0000

dnsdist (1.2.0-2) unstable; urgency=medium

  * Update Maintainer: as alioth is going away
  * Update Vcs-* URLs to point to salsa.debian.org
  * Bump Standards-Version to 4.1.3 (no changes)

 -- Chris Hofstaedtler <zeha@debian.org>  Thu, 18 Jan 2018 20:30:13 +0000

dnsdist (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0, fixes CVE-2016-7069, CVE-2017-7557.
    (Closes: #872854)
  * Install config example
  * Remove now-default options to dh
  * Force rebuild of dnslabeltext.cc
  * Update debian/copyright
  * Bump Standards-Version to 4.1.0

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 22 Aug 2017 09:47:47 +0000

dnsdist (1.1.0-2) unstable; urgency=medium

  * Bump debhelper compat to 10 for systemd support.
  * Drop our outdated copy of dnsdist.service, instead patch the
    upstream supplied one at install time to keep -u, -g.
  * Drop RestrictAddressFamilies from .service file on 32bit.
    This feature is broken in systemd before v233. (See also #849817)

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 31 Dec 2016 15:50:47 +0000

dnsdist (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0, upload to unstable.

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 29 Dec 2016 15:52:06 +0000

dnsdist (1.1.0~beta2-2) experimental; urgency=medium

  * Build with LDFLAGS=-latomic to fix FTBFS on powerpc

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 18 Dec 2016 14:11:59 +0000

dnsdist (1.1.0~beta2-1) experimental; urgency=medium

  * New upstream (beta) release, target experimental.
  * Enable protobuf, re2, systemd integration features.

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 17 Dec 2016 19:42:08 +0000

dnsdist (1.0.0-2) unstable; urgency=medium

  * Move package to pkg-dns team.
  * Bump Standards-Version to 3.9.8.
  * Enable extra hardening features for daemons.
  * Sort included html files to improve reproducibility.

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 03 Jul 2016 13:40:56 +0200

dnsdist (1.0.0-1) unstable; urgency=medium

  * New upstream version.
  * debian/watch: fix versionmangle substiution confusion

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 21 Apr 2016 13:45:37 +0000

dnsdist (1.0.0~beta1-1) unstable; urgency=medium

  * New upstream version.
  * Use https site in debian/watch.

 -- Christian Hofstaedtler <zeha@debian.org>  Fri, 15 Apr 2016 16:17:47 +0000

dnsdist (1.0.0~alpha2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7 with no other changes
  * Add debian/watch file

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 24 Feb 2016 22:57:52 +0000

dnsdist (1.0.0~alpha2-1) unstable; urgency=medium

  * Initial release. (Closes: #813823)

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 13 Feb 2016 01:43:50 +0000
