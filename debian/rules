#!/usr/bin/make -f

# Turn on all hardening flags, as we're a networked daemon.
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

# Vendor and version
CXXFLAGS += -DPACKAGEVERSION='"$(DEB_VERSION).$(DEB_VENDOR)"'


%:
	dh $@

override_dh_auto_clean:
	dh_auto_clean
	rm -f dnslabeltext.cc

override_dh_auto_configure:
	chmod a+x debian/configure-helpers/*
	PATH=debian/configure-helpers/:$$PATH dh_auto_configure -- \
	  --sysconfdir=/etc/dnsdist \
	  --enable-systemd --with-systemd=/lib/systemd/system \
	  --enable-unit-tests \
	  --disable-silent-rules \
	  --enable-dnscrypt \
	  --enable-dns-over-https \
	  --enable-dns-over-tls \
	  --enable-dnstap \
	  --with-ebpf \
	  --with-gnutls=no \
	  --with-libsodium \
	  --with-libssl \
	  --with-lmdb \
	  --with-lua \
	  --with-net-snmp \
	  --with-nghttp2 \
	  --with-protobuf \
	  --with-re2 \
	  --with-service-user='_dnsdist' \
	  --with-service-group='_dnsdist' \
	  $(CONFIGURE_ARGS)

override_dh_auto_build-arch:
	dh_auto_build -- V=1


override_dh_auto_test:
	echo tests disabled, require working ipv4 stack

override_dh_install:
	dh_auto_install
	install -m 0640 debian/config/dnsdist.conf debian/dnsdist/etc/dnsdist/
	install -d debian/dnsdist/usr/share/dnsdist/snmp
	install -m 644 -t debian/dnsdist/usr/share/dnsdist/snmp DNSDIST-MIB.txt

override_dh_installexamples:
	cp dnsdist.conf-dist dnsdist.conf
	dh_installexamples
	rm -f dnsdist.conf

override_dh_fixperms:
	dh_fixperms
	chmod 0640 debian/dnsdist/etc/dnsdist/dnsdist.conf
